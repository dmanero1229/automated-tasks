﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using System.Configuration;
using System.IO;
using System.Net.Sockets;
using System.Net.Security;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Data;

namespace MemberResponseManager
{
    class Program
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        private static byte[] WriteBuffer = new byte[200];
        private static ASCIIEncoding enc = new ASCIIEncoding();
        private static NetworkStream netStream;
        private static StreamReader strReader;
        private static SqlConnection dbConnection;
        private static SqlCommand rsvpCommand;

        private enum ResponseEnum
        {
            Yes,
            No,
            Stop
        }

        private class MemberData
        {
            public int TournamentID { get; set; }
            public string MemberEmail { get; set; }
            public ResponseEnum Response { get; set; }
        }

        static int Main(string[] args)
        {
            log.Info("Starting Member Response Manager process...");
            log.Debug("Connecting to POP server to check for responses...");

            string hostName = ConfigurationManager.AppSettings["PopServer"];
            int port = Convert.ToInt32(ConfigurationManager.AppSettings["PopPort"]);
            string username = ConfigurationManager.AppSettings["SendEmailFrom"];
            string password = ConfigurationManager.AppSettings["AccountPassword"];

            try
            {
                TcpClient tcpClient = new TcpClient(hostName, port);
                netStream = tcpClient.GetStream();
                strReader = new StreamReader(netStream);

                SendCommand(null, "+OK", "Could not connect to email server");
                SendCommand($"USER {username}\r\n", "+OK", "Could not connect to email server");
                SendCommand($"PASS {password}\r\n", "+OK", "Could not authenticate credentials on email server");

                log.Debug("Connected to POP server successfully.");
                string result = SendCommand($"STAT\r\n", "+OK", "Failed to get email count")[0];

                //Response from STAT command is: +OK n xxxxx, where n is the number of emails and xxxxx the size. I only need "n"
                int pos = result.IndexOf(' ', 4);
                if (pos <= 4)
                    ReportToAdministrator($"Could not parse email count from STAT command. Value returned was: {result}", isError: true);

                int emailCount = Convert.ToInt32(result.Substring(4, pos - 4));
                if (emailCount == 0)
                {
                    log.Debug("No new email found on the server. Exiting...");
                }
                else
                {
                    log.Info($"Found {emailCount} new emails on the server.");
                    int ignoredEmailCount = 0;
                    dbConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["AceKingSuitedDB"].ConnectionString);
                    rsvpCommand = new SqlCommand("Tournament.spRSVP", dbConnection);
                    rsvpCommand.Parameters.Add("TournamentID", SqlDbType.Int);
                    rsvpCommand.Parameters.Add("MemberID", SqlDbType.Int);
                    rsvpCommand.Parameters.Add("EmailAddress", SqlDbType.NVarChar, 200);
                    rsvpCommand.Parameters.Add("IsParticipating", SqlDbType.Bit);
                    rsvpCommand.Parameters.Add("ControllerByMemberID", SqlDbType.Int);
                    rsvpCommand.Parameters.Add("ControllerUID", SqlDbType.VarChar, 25);
                    rsvpCommand.Parameters.Add("InviteeID", SqlDbType.Int);
                    rsvpCommand.Parameters.Add("RegistrantID", SqlDbType.Int);
                    rsvpCommand.Parameters.Add("ParentLogID", SqlDbType.BigInt);
                    dbConnection.Open();

                    for (int i = 1; i <= emailCount; i++)
                    {
                        List<string> emailLines = SendCommand($"RETR {i}\r\n", null, string.Empty);
                        MemberData memberData = ParseEmail(emailLines);
                        if (memberData == null)  //Email is NOT a response from a user. Ignore it and report it.
                            ignoredEmailCount++;
                        else 
                        {
                            //Process email and delete it.
                            ProcessMemberResponse(memberData);
                            SendCommand($"DELE {i}\r\n", "+OK", "Could not delete email server");
                        }
                    }

                    if (ignoredEmailCount > 0)
                        ReportToAdministrator($"MemberResponseManager found {ignoredEmailCount} emails in the response inbox that could not be processed.", isError: false);
                }
                SendCommand("QUIT\r\n", "+OK", "Could not disconnect from email server");
                log.Info("Process completed successfully.");

                netStream.Close();
                strReader.Close();
                tcpClient.Close();

                Console.ReadLine();
                return 0;
            }
            catch (Exception ex)
            {
                ReportToAdministrator(ex.Message, isError: true);
                return 1;
            }
        }

        private static void ProcessMemberResponse(MemberData memberData)
        {
            //Console.WriteLine($"Found response: {memberData.Response} from {memberData.MemberEmail} for tournament: {memberData.TournamentID}");
            rsvpCommand.Parameters["TournamentID"].Value = memberData.TournamentID;
            rsvpCommand.Parameters["EmailAddress"].Value = memberData.MemberEmail;
            rsvpCommand.Parameters["IsParticipating"].Value = memberData.Response == ResponseEnum.Yes;
            rsvpCommand.Parameters["ControlledByMemberID"].Value = ConfigurationManager.AppSettings["ManagerMemberID"];
            rsvpCommand.Parameters["ControllerUID"].Value = "000000000000000";
            rsvpCommand.ExecuteNonQuery();
        }

        private static MemberData ParseEmail(List<string> emailLines)
        {
            //Need to find a line that starts with YES or NO and also one that has TID:xxxx to get the tournament ID
            string emailFrom = null;
            int tournamentID = 0;
            ResponseEnum? response = null;
            Regex tournamentIDRegex = new Regex(@"^.*(TID:\d*).*$");
            Regex emailRegex = new Regex(@"^\w+@\w+\.[A-Za-z]+$");
            Regex emailWithName = new Regex(@"^.*<\w +@\w +\.[A-Za-z]+>.*$");

            foreach (var line in emailLines)
            {
                if (emailFrom == null && line.ToUpper().StartsWith("FROM:"))
                {
                    string fromLine = line.Substring(5).Trim();
                    try
                    {
                        MailAddress email = new MailAddress(fromLine);
                        emailFrom = email.Address;
                        continue;
                    }
                    catch (Exception) {}
                }
                if (response == null)
                {
                    if (line.ToUpper() == "YES")
                        response = ResponseEnum.Yes;
                    else if (line.ToUpper() == "NO")
                        response = ResponseEnum.No;
                    else if (line.ToUpper() == "STOP")
                        response = ResponseEnum.Stop;
                }
                if (tournamentID == 0 && tournamentIDRegex.IsMatch(line))
                {
                    int pos1 = line.IndexOf("(TID:");
                    int pos2 = line.IndexOf(")", pos1 + 4);
                    if (pos2 > pos1 && pos1 >= 0)
                        int.TryParse(line.Substring(pos1 + 5, pos2 - pos1 - 5), out tournamentID);
                }
                if (emailFrom != null && tournamentID != 0 && response != null)
                    break;
            }
            if (emailFrom != null && tournamentID != 0 && response != null)
            {
                return new MemberData { MemberEmail = emailFrom, Response = response.Value, TournamentID = tournamentID };
            }
            return null;
        }

        private static List<string> SendCommand(string command, string expectedResponse, string errorToReport)
        {
            if (command != null)
            {
                WriteBuffer = enc.GetBytes(command);
                netStream.Write(WriteBuffer, 0, WriteBuffer.Length);
            }
            string response = strReader.ReadLine();
            if (expectedResponse != null)
            {
                if (!response.StartsWith(expectedResponse))
                {
                    ReportToAdministrator($"{errorToReport}: {response}", isError: true);
                    Environment.Exit(1);
                }
                return new List<string> { response };
            }
            else
            {
                //Multiple responses end with a "."
                List<string> responses = new List<string>();
                while (response != ".")
                {
                    responses.Add(response);
                    response = strReader.ReadLine();
                }
                return responses;
            }
        }

        private static void ReportToAdministrator(string message, bool isError)
        {
            string emailAddresses = ConfigurationManager.AppSettings["SystemAdminEmail"];
            string smtpServer = ConfigurationManager.AppSettings["SMTPServer"];
            int smtpPort = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);
            string sendEmailFrom = ConfigurationManager.AppSettings["SendEmailFrom"];
            SmtpClient mailClient = new SmtpClient();
            if (isError)
                log.ErrorFormat("Error found: {0}. Sending email to admins...", message);
            try
            {
                mailClient.Send(sendEmailFrom, emailAddresses, string.Format("AKS MemberResponseManager {0}", isError ? "error" : "notification"), message);
                log.Info("Notification sent to admins.");
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Could not notify administrators. Error found: {0}", ex.Message);
            }
        }
    }
}
