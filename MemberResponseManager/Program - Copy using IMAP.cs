﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using System.Configuration;
using System.IO;
using System.Net.Sockets;
using System.Net.Security;

namespace MemberResponseManager
{
    class Program
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        static StreamWriter sw = null;
        static TcpClient tcpc = null;
        static SslStream ssl = null;
        static string username, password;
        static string path;
        static int bytes = -1;
        static byte[] buffer;
        static StringBuilder sb = new StringBuilder();
        static byte[] dummy;

        static void Main(string[] args)
        {
            log.Info("Starting Member Response Manager process...");
            log.Debug("Connecting to POP server to check for responses...");
            try
            {
                path = Environment.CurrentDirectory + "\\emailresponse.txt";

                if (File.Exists(path))
                    File.Delete(path);

                sw = new StreamWriter(File.Create(path));

                // there should be no gap between the imap command and the \r\n       
                // ssl.read() -- while ssl.readbyte!= eof does not work because there is no eof from server 
                // cannot check for \r\n because in case of larger response from server ex:read email message 
                // there are lot of lines so \r \n appears at the end of each line 
                //ssl.timeout sets the underlying tcp connections timeout if the read or write 
                //time out exceeds then the undelying connection is closed 
                string iMapServer = ConfigurationManager.AppSettings["IMAPServer"];

                tcpc = new TcpClient(iMapServer, 993);

                ssl = new SslStream(tcpc.GetStream());
                ssl.AuthenticateAsClient(iMapServer);
                receiveResponse("");

                username = ConfigurationManager.AppSettings["SendEmailFrom"];
                password = ConfigurationManager.AppSettings["AccountPassword"];

                receiveResponse("$ LOGIN " + username + " " + password + "  \r\n");
                Console.Clear();

                receiveResponse("$ LIST " + "\"\"" + " \"*\"" + "\r\n");

                receiveResponse("$ SELECT INBOX\r\n");

                receiveResponse("$ STATUS INBOX (MESSAGES)\r\n");


                Console.WriteLine("enter the email number to fetch :");
                int number = int.Parse(Console.ReadLine());

                receiveResponse("$ FETCH " + number + " body[header]\r\n");
                receiveResponse("$ FETCH " + number + " body[text]\r\n");


                receiveResponse("$ LOGOUT\r\n");
            }
            catch (Exception ex)
            {
                Console.WriteLine("error: " + ex.Message);
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                    sw.Dispose();
                }
                if (ssl != null)
                {
                    ssl.Close();
                    ssl.Dispose();
                }
                if (tcpc != null)
                {
                    tcpc.Close();
                }
            }


            Console.ReadKey();
        }
        static void receiveResponse(string command)
        {
            try
            {
                if (command != "")
                {
                    if (tcpc.Connected)
                    {
                        dummy = Encoding.ASCII.GetBytes(command);
                        ssl.Write(dummy, 0, dummy.Length);
                    }
                    else
                    {
                        throw new ApplicationException("TCP CONNECTION DISCONNECTED");
                    }
                }
                ssl.Flush();


                buffer = new byte[2048];
                bytes = ssl.Read(buffer, 0, 2048);
                sb.Append(Encoding.ASCII.GetString(buffer));


                Console.WriteLine(sb.ToString());
                sw.WriteLine(sb.ToString());
                sb = new StringBuilder();

            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        }
    }
}
