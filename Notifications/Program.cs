﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using log4net;
using System.Net.Mail;
using System.Resources;
using System.Globalization;
using System.IO;
using System.Net;
using static System.Configuration.ConfigurationManager;

namespace Notifications
{
    class Program
    {
        private static ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType.Name);
        static SmtpClient mailClient;
        static string sendEmailFrom = AppSettings["SendEmailFrom"];
        static List<string> fields = new List<string>(new string[] { "LocationName", "BuyInAmount", "StartingStackSize", "LateRegistrationIsAllowedUntil",
                    "RebuyIsAllowedUntil", "SmallBlind", "BigBlind" });
        static ResourceManager resourceManager;

        static void Main(string[] args)
        {
            log.Info("Starting Notifications process...");
            try
            {
                using (SqlConnection dbConnection = new SqlConnection(ConnectionStrings["AceKingSuitedDB"].ConnectionString))
                {
                    SqlCommand cmd = new SqlCommand("exec Communication.spListTournamentsPendingReminders null, null, null", dbConnection);
                    dbConnection.Open();
                    SqlDataReader tournamentReader = cmd.ExecuteReader();

                    if (tournamentReader.HasRows)
                    {
                        var tournaments = new DataTable();
                        tournaments.Load(tournamentReader);
                        tournamentReader.Close();

                        cmd = new SqlCommand("Communication.spListMembersPendingReminders", dbConnection);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("TournamentID", SqlDbType.Int);
                        cmd.Parameters.Add("ReminderOrderInd", SqlDbType.TinyInt);
                        int managerMemberID = Convert.ToInt32(AppSettings["ManagerMemberID"]);
                        cmd.Parameters.Add("ManagerMemberID", SqlDbType.Int);

                        string folderName = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
                        resourceManager = ResourceManager.CreateFileBasedResourceManager("Notifications.Strings", folderName, null);

                        string emailServer = AppSettings["EmailServer"];
                        int smtpPort = Convert.ToInt32(AppSettings["SmtpPort"]);
                        mailClient = new SmtpClient(emailServer, smtpPort);
                        mailClient.EnableSsl = Convert.ToBoolean(AppSettings["UseSSL"]);
                        mailClient.Credentials = new NetworkCredential(sendEmailFrom, AppSettings["AccountPassword"]);
                        cmd.Parameters[2].Value = managerMemberID;

                        foreach (DataRow row in tournaments.Rows)
                        {
                            int tournamentID = (int)row["TournamentID"];
                            int reminderOrderInd = Convert.ToInt32(row["ReminderOrderInd"]);
                            Console.WriteLine("Processing ReminderOrderInd {0} for tournament ID: {1}...", reminderOrderInd, tournamentID);
                            cmd.Parameters[0].Value = tournamentID;
                            cmd.Parameters[1].Value = reminderOrderInd;

                            SqlDataReader memberReader = cmd.ExecuteReader();
                            if (memberReader.HasRows)
                            {
                                while (memberReader.Read())
                                {
                                    int communicationType = Convert.ToInt32(memberReader["CommunicationTypeID"]);
                                    string address = Convert.ToString(memberReader["EmailAddress"]);
                                    if ((communicationType & 1) != 0 && !string.IsNullOrWhiteSpace(address))  // Email
                                        SendEmail(address, memberReader, row, isText: false);

                                    if ((communicationType & 2) != 0)  // Phone
                                    {
                                        address = ((string)memberReader["Format"])?.Replace("[number]", (string)memberReader["PhoneNumber"]) ?? string.Empty;
                                        if (address != string.Empty)
                                            SendEmail(address, memberReader, row, isText: true);
                                    }
                                }
                                memberReader.Close();
                                //All notifications have been sent. Mark the tournament as being notified already.
                                // Since both procs have the same params, I'm reusing the same SqlCommand object
                                cmd.CommandText = "Communication.spUpdateTournamentReminder";
                                cmd.ExecuteNonQuery();
                                cmd.CommandText = "Communication.spListMembersPendingReminders";
                            }
                            else
                                log.Info("Found no members to notify for this reminder.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                SendErrorToAdmins(ex.Message);
            }

        }

        private static void SendEmail(string address, SqlDataReader memberReader, DataRow row, bool isText)
        {
            try
            {
                // TODO: Culture should come with each member
                string[] cultures = { "en", "es" };
                CultureInfo culture = new CultureInfo(cultures[1]);

                string memberName = (string)memberReader["NickName"];
                bool isReminder = Convert.ToBoolean(memberReader["IsReminder"]);
                string bodyTemplate = resourceManager.GetString(isText ? "PhoneTextBody" : "EmailBody", culture)
                        .Replace("@LateRegistrationAllowed", resourceManager.GetString(Convert.ToInt32(row["LateRegistrationIsAllowedUntil"]) > 0 ? "LateRegistrationAllowed" : "LateRegistrationNotAllowed", culture))
                        .Replace("@RebuysAllowed", resourceManager.GetString(Convert.ToInt32(row["RebuyIsAllowedUntil"]) > 0 ? "RebuysAllowed" : "RebuysNotAllowed", culture))
                        .Replace("@NickName", memberName);

                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(sendEmailFrom);
                msg.To.Add(address);
                msg.Subject = isText ? "" : resourceManager.GetString(isReminder ? "ReminderSubject" : "InvitationSubject", culture);
                foreach (var field in fields)
                    bodyTemplate = bodyTemplate.Replace($"@{field}", row[field].ToString());
                msg.Body = bodyTemplate.Replace("@Bounties", Convert.ToInt32(row["BountyAmount"]) == 0 ? resourceManager.GetString("NoBounties", culture) : $"Bounties: ${row["BountyAmount"]}")
                                       .Replace("@StartDate", isText ? row["StartDate"].ToString() : Convert.ToDateTime(row["StartDate"]).ToString("D", culture))
                                       .Replace(@"\n", "\r\n");

                mailClient.ServicePoint.MaxIdleTime = 1;
                mailClient.Send(msg);
            }
            catch (Exception ex)
            {
                SendErrorToAdmins(ex.Message);
            }
        }

        private static void SendErrorToAdmins(string errorMessage)
        {
            string emailAddresses = AppSettings["SystemAdminEmail"];
            log.ErrorFormat("Error found: {0}. Sending email to admins...", errorMessage);
            try
            {
                mailClient.Send(sendEmailFrom, emailAddresses, "Error sending notifications", errorMessage);
                log.Info("Error sent to admins.");
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Could not notify administrators. Error found: {0}", ex.Message);
            }
        }
    }
}
